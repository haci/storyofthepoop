package com.mobaxe.poopstory.screens;

import com.badlogic.gdx.Screen;
import com.mobaxe.poopstory.PoopStory;

public enum MyScreens {

	GAME_SCREEN {
		public Screen getScreenInstance() {
			return new GameScreen(PoopStory.actionResolver);
		}
	},
	GAME_OVER_SCREEN {
		public Screen getScreenInstance() {
			return new GameOverScreen();
		}
	},
	SPLASH_SCREEN {
		public Screen getScreenInstance() {
			return new SplashScreen();
		}
	},
	LEVEL_SCREEN {
		public Screen getScreenInstance() {
			return new LevelSelectionScreen();
		}
	};
	public abstract Screen getScreenInstance();

}
