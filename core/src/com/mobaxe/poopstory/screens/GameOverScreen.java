package com.mobaxe.poopstory.screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.poopstory.helpers.Assets;
import com.mobaxe.poopstory.ui.HomeButton;
import com.mobaxe.poopstory.ui.MoreButton;
import com.mobaxe.poopstory.ui.RefreshButton;
import com.mobaxe.poopstory.ui.RateButton;
import com.mobaxe.poopstory.utils.Utils;

public class GameOverScreen implements Screen {

	private Stage stage;
	private Table bgTable;
	private Texture bgTexture;
	private Image bgImage;

	private RefreshButton pb;
	private RateButton rb;
	private MoreButton mb;
	private HomeButton hb;
	private ArrayList<Table> buttonTables;

	public GameOverScreen() {
		buttonTables = new ArrayList<Table>();
		bgTable = new Table();
		bgTable.setFillParent(true);
		for (int i = 0; i < 4; i++) {
			buttonTables.add(new Table());
			buttonTables.get(i).setFillParent(true);
		}
		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);

	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		setBackgroundImage();
		createButtons();
	}

	private void createButtons() {
		pb = new RefreshButton();
		rb = new RateButton();
		mb = new MoreButton();
		hb = new HomeButton();

		buttonTables.get(0).add(pb).pad(0, 0, 300, 100);
		buttonTables.get(1).add(hb).pad(0, 95, 210, 0);
		buttonTables.get(2).add(rb).pad(0, 0, 100, 40);
		buttonTables.get(3).add(mb).pad(0, 0, 350, 630);

		stage.addActor(bgTable);

		for (Table table : buttonTables) {
			stage.addActor(table);
		}
	}

	private void setBackgroundImage() {
		bgTexture = Assets.bgGameOver;
		bgImage = new Image(bgTexture);
		bgTable.add(bgImage);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
