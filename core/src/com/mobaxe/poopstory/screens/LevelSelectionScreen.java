package com.mobaxe.poopstory.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.poopstory.PoopStory;
import com.mobaxe.poopstory.helpers.Assets;
import com.mobaxe.poopstory.ui.LevelButtons;
import com.mobaxe.poopstory.utils.Utils;

public class LevelSelectionScreen implements Screen {

	private Stage stage;
	private Table bgTable;
	private Table firstLine;
	private Table secondLine;
	private Table thirdLine;
	private Texture bgTexture;
	private Sprite bgSprite;
	private Image bgImage;

	public LevelSelectionScreen() {

		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));
		bgTable = new Table();
		firstLine = new Table();
		secondLine = new Table();
		thirdLine = new Table();

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		setBackgroundImage();
		createButtons();
	}

	private void createButtons() {

		bgTable.setFillParent(true);
		firstLine.setFillParent(true);
		secondLine.setFillParent(true);
		thirdLine.setFillParent(true);

		// LEVEL 1
		LevelButtons l1 = new LevelButtons(1);
		LevelButtons l2 = new LevelButtons(2);
		if (!PoopStory.getPrefs().getLOCK(2)) {
			l2.setColor(1, 1, 1, 0.7f);
			l2.setTouchable(Touchable.disabled);
		}

		LevelButtons l3 = new LevelButtons(3);
		if (!PoopStory.getPrefs().getLOCK(3)) {
			l3.setColor(1, 1, 1, 0.7f);
			l3.setTouchable(Touchable.disabled);
		}
		LevelButtons l4 = new LevelButtons(4);
		if (!PoopStory.getPrefs().getLOCK(4)) {
			l4.setColor(1, 1, 1, 0.7f);
			l4.setTouchable(Touchable.disabled);
		}

		firstLine.add(l1).pad(0, 0, 180, 10);
		firstLine.add(l2).pad(0, 0, 180, 10);
		firstLine.add(l3).pad(0, 0, 180, 10);
		firstLine.add(l4).pad(0, 0, 180, 10);

		// LEVEL 2
		LevelButtons l5 = new LevelButtons(5);
		if (!PoopStory.getPrefs().getLOCK(5)) {
			l5.setColor(1, 1, 1, 0.7f);
			l5.setTouchable(Touchable.disabled);
		}
		LevelButtons l6 = new LevelButtons(6);
		if (!PoopStory.getPrefs().getLOCK(6)) {
			l6.setColor(1, 1, 1, 0.7f);
			l6.setTouchable(Touchable.disabled);
		}
		LevelButtons l7 = new LevelButtons(7);
		if (!PoopStory.getPrefs().getLOCK(7)) {
			l7.setColor(1, 1, 1, 0.7f);
			l7.setTouchable(Touchable.disabled);
		}
		LevelButtons l8 = new LevelButtons(8);
		if (!PoopStory.getPrefs().getLOCK(8)) {
			l8.setColor(1, 1, 1, 0.7f);
			l8.setTouchable(Touchable.disabled);
		}
		secondLine.add(l5).pad(220, 0, 180, 10);
		secondLine.add(l6).pad(0, 0, -40, 10);
		secondLine.add(l7).pad(0, 0, -40, 10);
		secondLine.add(l8).pad(0, 0, -40, 10);

		// LEVEL 3
		LevelButtons l9 = new LevelButtons(9);
		if (!PoopStory.getPrefs().getLOCK(9)) {
			l9.setColor(1, 1, 1, 0.7f);
			l9.setTouchable(Touchable.disabled);
		}
		LevelButtons l10 = new LevelButtons(10);
		if (!PoopStory.getPrefs().getLOCK(10)) {
			l10.setColor(1, 1, 1, 0.7f);
			l10.setTouchable(Touchable.disabled);
		}
		LevelButtons l11 = new LevelButtons(11);
		if (!PoopStory.getPrefs().getLOCK(11)) {
			l11.setColor(1, 1, 1, 0.7f);
			l11.setTouchable(Touchable.disabled);
		}
		LevelButtons l12 = new LevelButtons(12);
		if (!PoopStory.getPrefs().getLOCK(12)) {
			l12.setColor(1, 1, 1, 0.7f);
			l12.setTouchable(Touchable.disabled);
		}

		// thirdLine.pad(top, left, bottom, right)
		thirdLine.add(l9).pad(255, 5, 0, 10);
		thirdLine.add(l10).pad(220, 0, -40, 10);
		thirdLine.add(l11).pad(0, 0, -260, 10);
		thirdLine.add(l12).pad(0, 0, -260, 10);

		bgImage = new Image(bgTexture);
		bgTable.add(bgImage);
		stage.addActor(bgTable);
		stage.addActor(firstLine);
		stage.addActor(secondLine);
		stage.addActor(thirdLine);
		stage.addAction(Actions.sequence(Actions.moveBy(-800, 0), Actions.moveTo(0, 0, 0.5f)));

	}

	private void setBackgroundImage() {
		bgTexture = Assets.screenBg;

		bgSprite = new Sprite(bgTexture);
		bgSprite.setColor(0, 0, 0, 0);
		bgSprite.setX(Utils.virtualWidth - bgSprite.getWidth() / 2);
		bgSprite.setY(Utils.virtualHeight - bgSprite.getHeight() / 2);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}