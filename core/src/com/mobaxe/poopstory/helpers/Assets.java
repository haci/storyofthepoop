package com.mobaxe.poopstory.helpers;

import java.util.ArrayList;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class Assets {

	private static FreeTypeFontGenerator generator;
	private static FreeTypeFontParameter parameter;
	public static BitmapFont font;

	private static Sound sound;
	private static Music music;

	public static ArrayList<Texture> levels = new ArrayList<Texture>();
	public static Texture screenBg, thorn, bgGameOver, homeBtn, refreshBtn, starBtn, dotsBtn, splash;

	public static Texture loadTexture(String filePath) {
		return new Texture(Gdx.files.internal(filePath));
	}

	public static Sound loadSound(String filePath) {
		sound = Gdx.audio.newSound(Gdx.files.internal(filePath));
		return sound;
	}

	public static Music loadMusic(String filePath) {
		music = Gdx.audio.newMusic(Gdx.files.internal(filePath));
		return music;
	}

	public static FileHandle loadJson(String filePath) {
		return Gdx.files.internal(filePath);
	}

	public static TextureAtlas loadAtlas(String filePath) {
		return new TextureAtlas(Gdx.files.internal(filePath));
	}

	public static void loadTexturesOnCreate() {

		if (levels.size() > 0) {
			levels.clear();
		}
		thorn = loadTexture("images/thorn.png");
		levels.add(loadTexture("ui/1.png"));
		levels.add(loadTexture("ui/2.png"));
		levels.add(loadTexture("ui/3.png"));
		levels.add(loadTexture("ui/4.png"));
		levels.add(loadTexture("ui/5.png"));
		levels.add(loadTexture("ui/6.png"));
		levels.add(loadTexture("ui/7.png"));
		levels.add(loadTexture("ui/8.png"));
		levels.add(loadTexture("ui/9.png"));
		levels.add(loadTexture("ui/10.png"));
		levels.add(loadTexture("ui/11.png"));
		levels.add(loadTexture("ui/12.png"));
		// STAGE
		screenBg = loadTexture("ui/bg.jpg");
		bgGameOver = loadTexture("images/gameover.jpg");
		// bgGameOver, homeBtn, refreshBtn, starBtn, dotsBtn
		homeBtn = loadTexture("images/home.png");
		refreshBtn = loadTexture("images/refresh.png");
		starBtn = loadTexture("images/star.png");
		dotsBtn = loadTexture("images/dots.png");
		splash = loadTexture("images/splash.png");
		// FONT
		generateFont();

	}

	private static void generateFont() {
		generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/text.ttf"));
		parameter = new FreeTypeFontParameter();
		int density = 40;
		if (ApplicationType.Android == Gdx.app.getType()) {
			density = 8;
		}

		parameter.size = (int) (density * Gdx.graphics.getDensity());
		font = generator.generateFont(parameter);

		generator.dispose(); // don't forget to dispose to avoid memory leaks!

	}

	public static void dispose() {
	}
}