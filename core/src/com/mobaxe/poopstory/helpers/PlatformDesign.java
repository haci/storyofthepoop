package com.mobaxe.poopstory.helpers;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.mobaxe.poopstory.gameobjects.Platform;
import com.mobaxe.poopstory.managers.GameManager;
import com.mobaxe.poopstory.managers.LevelGenerator;

public class PlatformDesign {

	private ArrayList<Platform> plats;
	private float scl;
	private float scl1;
	private Vector2 sprSize;
	private Vector2 sprSize1;
	private Vector2 sprSize2;
	private float scl2;
	private BoxProperties box;
	private CustomSprite boxSpr;

	public void createBox(float x, float y) {
		float sprWidth = 1f;
		float sprHeight = 1f;
		float boxWidth = 0.3f;
		float boxHeight = 0.3f;
		Vector2 position = new Vector2(x, y);
		boxSpr = new CustomSprite(Assets.thorn, sprWidth, sprHeight);
		box = new BoxProperties(GameManager.world, boxWidth, boxHeight, position, BodyType.StaticBody, true);
		boxSpr.setPosition(box.getBody().getPosition().x - 0.37f, box.getBody().getPosition().y - 0.45f);
		box.getBody().setUserData(boxSpr);
	}

	public PlatformDesign(int level) {
		plats = new ArrayList<Platform>();

		switch (level) {
		case 2:
			sprSize = new Vector2(3f, 3f);
			sprSize1 = new Vector2(2f, 2f);
			scl = 3.2f;
			scl1 = 2.2f;
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-14, 2), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-7, -2), sprSize, scl));
			createBox(-5, 1f);
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(3, 3), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-7, -10), sprSize, scl));
			createBox(2, -7);
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(0, -10), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(3, -8), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(6, -6), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(3, -2), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-1, -1), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("door", new Vector2(2.7f, 4.9f), new Vector2(2.5f,
					3.2f), 1.5f));
			break;
		case 3:

			sprSize = new Vector2(3f, 3f);
			scl = 3.2f;
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-14, 2), sprSize, scl));
			createBox(-5, 1f);
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-7, -2), sprSize, scl));
			createBox(-2, 3f);
			createBox(0, -4f);
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-4, 0), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-1, 2), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(6, -2), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-1, -7), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-5, -9), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("door", new Vector2(-4.7f, -6f), new Vector2(2.5f,
					3.2f), 1.5f));
			break;
		case 4:
			sprSize1 = new Vector2(2f, 2f);
			scl1 = 2.2f;
			sprSize = new Vector2(5.2f, 3f);
			scl = 5.5f;
			sprSize2 = new Vector2(3.4f, 3f);
			scl2 = 3.4f;
			plats.add((Platform) LevelGenerator.generate("platform", new Vector2(-14, 2), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("platform3", new Vector2(-7, -11), sprSize2, scl2));
			createBox(-6, -7.6f);
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-2, -10), sprSize2, scl2));
			createBox(0.4f, -7f);
			plats.add((Platform) LevelGenerator.generate("platform3", new Vector2(3, -11), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(8, -9), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(5.5f, -4), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(8.5f, -1), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(12, -2), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("door", new Vector2(12.5f, 0.8f), new Vector2(2.5f,
					3.2f), 1.5f));
			break;
		case 5:
			sprSize1 = new Vector2(2f, 2f);
			scl1 = 2.2f;
			sprSize = new Vector2(5.2f, 3f);
			scl = 5.5f;
			sprSize2 = new Vector2(3.4f, 3f);
			scl2 = 3.4f;
			plats.add((Platform) LevelGenerator.generate("line2", new Vector2(-17.8f, -11.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line3", new Vector2(-17.8f, -10.9f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("platform", new Vector2(-10, -5), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-4, -8), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform", new Vector2(1, -5), sprSize, scl));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(7, -4), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(4, 0), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(8, 2), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(4, 4.8f), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-7, 1), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("door", new Vector2(-7.2f, 2.85f), new Vector2(2.5f,
					3.2f), 1.5f));
			break;
		case 6:
			sprSize1 = new Vector2(2f, 2f);
			scl1 = 2.2f;
			sprSize = new Vector2(5.2f, 3f);
			scl = 5.5f;
			sprSize2 = new Vector2(3.4f, 3f);
			scl2 = 3.4f;
			plats.add((Platform) LevelGenerator.generate("line2", new Vector2(-17.8f, -11.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line3", new Vector2(-17.8f, -10.9f), sprSize, 35));
			createBox(-1f, 4.5f);
			createBox(2.3f, 5.4f);
			plats.add((Platform) LevelGenerator.generate("platform", new Vector2(-11, 1), sprSize, scl));
			createBox(-8f, 4);
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-7, 4), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(4.5f, 0), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(7.9f, 3), sprSize1, scl1));
			createBox(-1.5f, -1.7f);
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(4, 4.8f), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-2.5f, -5), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-7.5f, -8), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("door", new Vector2(-7f, -5.2f), new Vector2(2.5f,
					3.2f), 1.5f));
			break;
		case 7:
			sprSize1 = new Vector2(2f, 2f);
			scl1 = 2.2f;
			sprSize = new Vector2(5.2f, 3f);
			scl = 5.5f;
			sprSize2 = new Vector2(3.4f, 3f);
			scl2 = 3.4f;
			plats.add((Platform) LevelGenerator.generate("line2", new Vector2(-17.8f, -11.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line3", new Vector2(-17.8f, -10.9f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line4", new Vector2(-16.8f, -10.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-7.5f, -8), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-2.5f, -5), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(3.5f, -2.3f), sprSize2,
					scl2));
			plats.add((Platform) LevelGenerator
					.generate("platform2", new Vector2(5.1f, 1.5f), sprSize1, scl1));
			plats.add((Platform) LevelGenerator
					.generate("platform2", new Vector2(5.7f, 6.3f), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("door", new Vector2(13.7f, 7f), new Vector2(2.5f,
					3.2f), 1.5f));
			break;
		case 8:
			sprSize1 = new Vector2(2f, 2f);
			scl1 = 2.2f;
			sprSize = new Vector2(5.2f, 3f);
			scl = 5.5f;
			sprSize2 = new Vector2(3.4f, 3f);
			scl2 = 3.4f;
			plats.add((Platform) LevelGenerator.generate("line2", new Vector2(-17.8f, -11.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line3", new Vector2(-17.8f, -10.9f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line4", new Vector2(-16.8f, -10.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line4", new Vector2(-18.8f, -21.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-9.5f, 1), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(1.2f, -2.3f), sprSize2,
					scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(1.7f, -8.3f), sprSize1,
					scl1));
			plats.add((Platform) LevelGenerator
					.generate("platform2", new Vector2(8.1f, 1.7f), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(3.6f, 6f), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("door", new Vector2(1.5f, -6.5f), new Vector2(2.5f,
					3.2f), 1.5f));
			break;
		case 9:
			sprSize1 = new Vector2(2f, 2f);
			scl1 = 2.2f;
			sprSize = new Vector2(5.2f, 3f);
			scl = 5.5f;
			sprSize2 = new Vector2(3.4f, 3f);
			scl2 = 3.4f;
			plats.add((Platform) LevelGenerator.generate("line2", new Vector2(-17.8f, -11.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line3", new Vector2(-17.8f, -10.9f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line4", new Vector2(-16.8f, -10.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line4", new Vector2(-18.8f, -21.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-9.5f, 1), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-5.2f, 5), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-3.2f, -2.3f), sprSize1,
					scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(4.7f, -5.7f), sprSize2,
					scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(0.5f, -3.7f), sprSize2,
					scl2));
			plats.add((Platform) LevelGenerator
					.generate("platform2", new Vector2(8.1f, 1.7f), sprSize1, scl1));
			createBox(1.5f, -0.5f);
			createBox(-1.5f, 4 );
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(3.6f, 6f), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("door", new Vector2(-3.3f, -0.5f), new Vector2(2.5f,
					3.2f), 1.5f));
			break;
		case 10:
			sprSize1 = new Vector2(2f, 2f);
			scl1 = 2.2f;
			sprSize = new Vector2(5.2f, 3f);
			scl = 5.5f;
			sprSize2 = new Vector2(3.4f, 3f);
			scl2 = 3.4f;
			plats.add((Platform) LevelGenerator.generate("line2", new Vector2(-17.8f, -11.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line3", new Vector2(-17.8f, -10.9f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line4", new Vector2(-16.8f, -10.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line4", new Vector2(-18.8f, -21.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-9.5f, -6), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(6.4f, 5f), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-5.2f, -2.3f), sprSize1,
					scl1));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(0.5f, -3.7f), sprSize2,
					scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(6.7f, -1.7f), sprSize2,
					scl2));
			createBox(8.5f, 1.4f);
			createBox(-0.5f, 4.2f);
			createBox(2.3f, 5.2f);
			plats.add((Platform) LevelGenerator.generate("door", new Vector2(1f, -1f), new Vector2(2.5f,
					3.2f), 1.5f));
			break;
		case 11:
			sprSize1 = new Vector2(2f, 2f);
			scl1 = 2.2f;
			sprSize = new Vector2(5.2f, 3f);
			scl = 5.5f;
			sprSize2 = new Vector2(3.4f, 3f);
			scl2 = 3.4f;
			plats.add((Platform) LevelGenerator.generate("line2", new Vector2(-17.8f, -11.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line3", new Vector2(-17.8f, -10.9f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line4", new Vector2(-16.8f, -10.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line4", new Vector2(-18.8f, -21.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-6.5f, -6), sprSize2, scl2));
			plats.add((Platform) LevelGenerator
					.generate("platform2", new Vector2(4.2f, 1.5f), sprSize1, scl1));
			plats.add((Platform) LevelGenerator
					.generate("platform2", new Vector2(5.2f, 6.8f), sprSize1, scl1));
			plats.add((Platform) LevelGenerator
					.generate("platform2", new Vector2(-1f, -3.7f), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(5f, -6f), sprSize2, scl2));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(8f, -1.8f), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("door", new Vector2(13.7f, 7f), new Vector2(2.5f,
					3.2f), 1.5f));
			break;
		case 12:
			sprSize1 = new Vector2(2f, 2f);
			scl1 = 2.2f;
			sprSize = new Vector2(5.2f, 3f);
			scl = 5.5f;
			sprSize2 = new Vector2(3.4f, 3f);
			scl2 = 3.4f;
			plats.add((Platform) LevelGenerator.generate("line2", new Vector2(-17.8f, -11.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line3", new Vector2(-17.8f, -10.9f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line4", new Vector2(-16.8f, -10.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("line4", new Vector2(-18.8f, -21.5f), sprSize, 35));
			plats.add((Platform) LevelGenerator.generate("platform2", new Vector2(-8.2f, -1), sprSize2, scl2));
			createBox(-6, 2.3f);
			createBox(-2.5f, 3f);
			createBox(-0.1f, 4.3f);
			createBox(2.5f, 5.4f);
			plats.add((Platform) LevelGenerator
					.generate("platform2", new Vector2(4.2f, 3.8f), sprSize1, scl1));
			plats.add((Platform) LevelGenerator.generate("door", new Vector2(13.7f, -4f), new Vector2(2.5f,
					3.2f), 1.5f));
			break;
		default:

		}
	}

	public ArrayList<Platform> getPlats() {
		return plats;
	}
}