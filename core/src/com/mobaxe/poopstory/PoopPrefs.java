package com.mobaxe.poopstory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class PoopPrefs {

	private String TOTAL_BALANCE = "totalbalance";
	private Preferences prefs = Gdx.app.getPreferences("poopPrefs");

	private String LOCK = "lock";

	public void saveLOCK(int level, boolean lock) {
		prefs.putBoolean(LOCK + level, lock);
		prefs.flush();
	}

	public boolean getLOCK(int level) {
		return prefs.getBoolean(LOCK + level);
	}

	public void saveTotalBalance(int amount) {
		prefs.putInteger(TOTAL_BALANCE, amount);
		prefs.flush();
	}

	public int getTotalBalance() {
		return prefs.getInteger(TOTAL_BALANCE);
	}

	public void clearPrefs() {
		prefs.clear();
	}

}
