package com.mobaxe.poopstory.managers;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.mobaxe.poopstory.gameobjects.Instance;
import com.mobaxe.poopstory.gameobjects.Level;
import com.mobaxe.poopstory.gameobjects.Platform;
import com.mobaxe.poopstory.helpers.Assets;
import com.mobaxe.poopstory.helpers.BodyEditorLoader;
import com.mobaxe.poopstory.helpers.Box2DFactory;
import com.mobaxe.poopstory.utils.Utils;

public class LevelGenerator {

	private static Sprite sprite;

	public static Instance generate(String name, Vector2 pos, Vector2 spriteSize, float scale) {
		BodyEditorLoader bodyEditorLoader = new BodyEditorLoader(Assets.loadJson("levels/" + name + ".json"));
		FixtureDef fixtureDef = Box2DFactory.createFixture(new PolygonShape(), 1, 0, 0, false);
		if (name.equals("door")) {
			fixtureDef.isSensor = true;
			fixtureDef.filter.groupIndex = Utils.DOOR;
		} else {
			fixtureDef.filter.groupIndex = Utils.TEST;
		}
		Body body = Box2DFactory.createBody(GameManager.world, BodyType.StaticBody, fixtureDef, pos, false);
		if (name.equals("door")) {
			body.setUserData("door");
		}
		bodyEditorLoader.attachFixture(body, name, fixtureDef, scale);
		Vector2 origin = bodyEditorLoader.getOrigin(name, scale);
		Vector2 position = body.getPosition().sub(origin);

		sprite = new Sprite(Assets.loadTexture("levels/" + name + ".png"));
		sprite.setSize(spriteSize.x, spriteSize.y);
		sprite.setOrigin(origin.x, origin.y);
		sprite.setPosition(position.x, position.y);

		if (name.startsWith("lvl")) {
			return new Level(body, sprite);
		} else {
			return new Platform(body, sprite);
		}
	}

}
