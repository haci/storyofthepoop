package com.mobaxe.poopstory.managers;

import java.util.ArrayList;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.mobaxe.poopstory.ActionResolver;
import com.mobaxe.poopstory.GameLevel;
import com.mobaxe.poopstory.PoopStory;
import com.mobaxe.poopstory.gameobjects.Level;
import com.mobaxe.poopstory.gameobjects.MiniMap;
import com.mobaxe.poopstory.gameobjects.Platform;
import com.mobaxe.poopstory.gameobjects.Poop;
import com.mobaxe.poopstory.gameobjects.Pop;
import com.mobaxe.poopstory.helpers.AnimatedSprite;
import com.mobaxe.poopstory.helpers.Assets;
import com.mobaxe.poopstory.helpers.BoxProperties;
import com.mobaxe.poopstory.helpers.CustomSprite;
import com.mobaxe.poopstory.helpers.PlatformDesign;
import com.mobaxe.poopstory.screens.MyScreens;
import com.mobaxe.poopstory.ui.JumpButton;
import com.mobaxe.poopstory.ui.LeftButton;
import com.mobaxe.poopstory.ui.RightButton;
import com.mobaxe.poopstory.utils.Utils;

public class GameManager {

	public static OrthographicCamera camera;
	public static InputMultiplexer multiplexer;
	public static SpriteBatch batch;
	public static Stage stage;
	public static World world;

	private static Table bgTable;
	private static ArrayList<Table> btnTables;
	private static Image bgImage;

	private static FileHandle file;
	private static Sprite sprite;
	private static AnimatedSprite animSprite;
	private static Vector2 pos;
	private static Vector2 spriteSize;
	private static String name;
	private static float scale;
	private static short category;
	private static BodyType bodyType;
	public static boolean isGameStarted;

	private static MiniMap map;
	private static Pop pop;
	public static Poop poop;

	public static boolean isJumpClicked = false;
	public static boolean isRightClicked = false;
	public static boolean isLeftClicked = false;

	private static LeftButton left;
	private static RightButton right;
	private static JumpButton jump;

	public static int gameLevel;

	public static int adCounter = Utils.INTERSTITIAL_FREQ - 1;
	public static ActionResolver actionResolver;
	public static boolean adIsShowed = false;

	private static Level level;
	private static ArrayList<Platform> platforms = new ArrayList<Platform>();

	public static void init(World w, OrthographicCamera c, Stage s, SpriteBatch sb, InputMultiplexer im,
			ActionResolver a) {
		world = w;
		camera = c;
		stage = s;
		batch = sb;
		multiplexer = im;
		actionResolver = a;
	}

	public static void initWorld() {
		if (platforms.size() != 0) {
			platforms.clear();
		}
		if (getCurrentLevel() == 1) {
			if (poop != null) {
			poop.body.setTransform(50, 50, 0);
			}
			createStartScreen();
			createFirstLevel();
		} else {
			createLevelsDynamically();
		}

	}

	@SuppressWarnings("unused")
	public static void createOuterWalls() {
		BoxProperties right = new BoxProperties(world, 1, camera.viewportHeight - 1, new Vector2(
				camera.viewportWidth / 2f, 0), BodyType.StaticBody, false);
		BoxProperties left = new BoxProperties(world, 1, camera.viewportHeight - 1, new Vector2(
				-camera.viewportWidth / 2f, 0), BodyType.StaticBody, false);
		BoxProperties bottom = new BoxProperties(world, camera.viewportWidth - 1, 2, new Vector2(0,
				-camera.viewportHeight / 1.3f), BodyType.StaticBody, false);
		bottom.getBody().setUserData("bottom");

	}

	private static void createRealPoop() {
		// POOP
		TextureAtlas atlas = Assets.loadAtlas("anim/poop.atlas");
		Animation anim = new Animation(0.25f, atlas.getRegions());
		anim.setPlayMode(PlayMode.LOOP);

		animSprite = new AnimatedSprite(anim, true);
		pos = new Vector2(-13f, 12);
		spriteSize = new Vector2(3f, 3f);
		category = Utils.POOP;
		bodyType = BodyType.DynamicBody;

		poop = new Poop(world, animSprite, bodyType, pos, spriteSize, category);
	}

	private static void createStartScreen() {
		bgTable = new Table();
		bgTable.setFillParent(true);
		bgImage = new Image(Assets.loadTexture("models/poopBG.jpg"));
		bgTable.add(bgImage);
		bgTable.setName("background");
		stage.addActor(bgTable);

		// MAP
		file = Assets.loadJson("models/bgModel.json");
		pos = new Vector2(-18.8f, -11f);
		spriteSize = new Vector2(0, 0);
		scale = 37.5f;
		name = "poopBG";
		category = Utils.TEST;
		bodyType = BodyType.StaticBody;

		map = new MiniMap(file, world, null, bodyType, pos, spriteSize, scale, name, category);

		// POP
		sprite = new Sprite(Assets.loadTexture("images/pop.png"));
		pos = new Vector2(-15.85f, 6);
		spriteSize = new Vector2(0.9f, 0.9f);
		category = Utils.TEST;
		bodyType = BodyType.DynamicBody;

		pop = new Pop(world, sprite, bodyType, pos, spriteSize, category);

	}

	public static void createLevelsDynamically() {

		for (Actor actor : stage.getActors()) {
			if (actor.getName().equals(bgTable.getName())) {
				actor.clear();
			}
		}
		bgTable = new Table();
		bgTable.setFillParent(true);
		bgImage = new Image(Assets.loadTexture("levels/lvl" + getCurrentLevel() + ".png"));
		bgTable.add(bgImage);
		bgTable.setName("background");
		stage.addActor(bgTable);

		btnTables = new ArrayList<Table>();
		Table tbl1 = new Table();
		Table tbl2 = new Table();
		Table tbl3 = new Table();
		btnTables.add(tbl1);
		btnTables.add(tbl2);
		btnTables.add(tbl3);

		left = new LeftButton();
		right = new RightButton();
		jump = new JumpButton();

		for (int i = 0; i < btnTables.size(); i++) {
			if (i == 0) {
				btnTables.get(i).add(left).pad(0, 150, 300, 0);
			} else if (i == 1) {
				btnTables.get(i).add(jump).pad(0, 1400, 155, 0);
			} else {
				btnTables.get(i).add(right).pad(0, 350, 155, 0);
			}
		}
		for (Table table : btnTables) {
			stage.addActor(table);
		}

		PlatformDesign map = new PlatformDesign(getCurrentLevel());
		platforms = map.getPlats();

		isGameStarted = true;

		createRealPoop();
		createOuterWalls();

	}

	public static void showIntersititial() {

		// Ads every X refresh!
		if (adCounter == Utils.INTERSTITIAL_FREQ) {
			if (!adIsShowed) {
				actionResolver.showOrLoadInterstital();
				adIsShowed = true;
				adCounter--;
			}
		} else if (adCounter == 0) {
			adCounter = Utils.INTERSTITIAL_FREQ;
		} else {
			adCounter--;
		}

	}

	private static void createFirstLevel() {
		Timer.schedule(new Task() {
			@Override
			public void run() {
				map.dispose();
				pop.dispose();
				for (Actor actor : stage.getActors()) {
					if (actor.getName().equals(bgTable.getName())) {
						actor.clear();
					}
				}

				bgTable = new Table();
				bgTable.setFillParent(true);
				bgImage = new Image(Assets.loadTexture("levels/lvl1.png"));
				bgTable.add(bgImage);
				bgTable.setName("background");
				stage.addActor(bgTable);

				btnTables = new ArrayList<Table>();
				btnTables.add(new Table());
				btnTables.add(new Table());
				btnTables.add(new Table());

				left = new LeftButton();
				right = new RightButton();
				jump = new JumpButton();

				for (int i = 0; i < btnTables.size(); i++) {
					if (i == 0) {
						btnTables.get(i).add(left).pad(0, 150, 300, 0);
					} else if (i == 1) {
						btnTables.get(i).add(jump).pad(0, 1400, 155, 0);
					} else {
						btnTables.get(i).add(right).pad(0, 350, 155, 0);
					}
				}
				for (Table table : btnTables) {
					stage.addActor(table);
				}

				Vector2 sprSize = new Vector2(5.2f, 3f);
				float scl = 5.5f;

				platforms.add((Platform) LevelGenerator.generate("platform", new Vector2(-14, 0), sprSize,
						scl));
				platforms.add((Platform) LevelGenerator
						.generate("platform", new Vector2(-8, 2), sprSize, scl));
				platforms.add((Platform) LevelGenerator.generate("platform", new Vector2(-2, -4), sprSize,
						scl));
				platforms.add((Platform) LevelGenerator.generate("line", new Vector2(9.4f, -2.5f), sprSize,
						35));
				platforms.add((Platform) LevelGenerator.generate("door", new Vector2(9.4f, -1.5f),
						new Vector2(2.5f, 3.2f), 3));

				isGameStarted = true;

				createRealPoop();
				createOuterWalls();
			}
		}, 2.6f);
	}

	public static void updateSprites(float delta) {
		batch.setProjectionMatrix(camera.combined);

		if (pop != null) {
			batch.begin();
			pop.draw(batch);
			batch.end();
		}

		if (level != null) {
			batch.begin();
			level.draw(batch);
			batch.end();
		}

		if (platforms.size() != 0) {
			batch.begin();
			for (Platform platform : platforms) {
				platform.draw(batch);
			}
			batch.end();
		}

		if (poop != null) {
			batch.begin();
			poop.draw(batch);
			batch.end();
		}

		Array<Body> tmpBodies = new Array<Body>();
		world.getBodies(tmpBodies);
		for (Body body : tmpBodies) {
			if (body.getUserData() != null && body.getUserData() instanceof Sprite) {
				Sprite sprite = (Sprite) body.getUserData();
				batch.begin();
				sprite.draw(batch);
				batch.end();
			}
		}

	}

	public static void updateStages(float delta) {
		stage.act(delta);
		stage.draw();
	}

	public static void createContactListener() {

		world.setContactListener(new ContactListener() {

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
			}

			@Override
			public void endContact(Contact contact) {
			}

			@Override
			public void beginContact(Contact contact) {

				if (isGameStarted) {
					Body bodyB = contact.getFixtureB().getBody();
					Body bodyA = contact.getFixtureA().getBody();
					// ON GROUND
					if (platforms.size() != 0) {
						for (Platform platform : platforms) {
							if (bodyA.equals(platform.getInstance()) && bodyB.equals(poop.body)) {
								poop.setOnGround(true);
							}
						}
					}
					// // ON FALLING DOWN
					if (bodyA.getUserData() != null && bodyA.getUserData().equals("bottom")) {
						ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
						ScreenManager.getInstance().show(MyScreens.GAME_OVER_SCREEN);
						isRightClicked = false;
						isLeftClicked = false;
						isJumpClicked = false;
						isGameStarted = false;
						showIntersititial();
					}
					// ON HIT THE THORN
					if (bodyA.getUserData() != null && !bodyA.getUserData().equals("door")
							&& !bodyA.getUserData().equals("bottom")) {
						ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
						ScreenManager.getInstance().show(MyScreens.GAME_OVER_SCREEN);
						isRightClicked = false;
						isLeftClicked = false;
						isJumpClicked = false;
						isGameStarted = false;
						showIntersititial();
					}
					// ON HIT DOOR
					if (bodyA.getUserData() != null && bodyA.getUserData().equals("door")) {
						PoopStory.getPrefs().saveLOCK(getCurrentLevel() + 1, true);

						ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
						ScreenManager.getInstance().show(MyScreens.LEVEL_SCREEN);
						isRightClicked = false;
						isLeftClicked = false;
						isJumpClicked = false;
						isGameStarted = false;
						showIntersititial();
					}

				}
			}
		});
	}

	public static int getCurrentLevel() {
		return gameLevel;
	}

	public static void setCurrentLevel(int level) {
		gameLevel = level;
	}
}
