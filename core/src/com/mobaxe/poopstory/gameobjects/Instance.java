package com.mobaxe.poopstory.gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;

public interface Instance {

	public void draw(SpriteBatch batch);

	public void dispose(Body body);

}
