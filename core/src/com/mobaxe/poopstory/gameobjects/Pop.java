package com.mobaxe.poopstory.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import com.mobaxe.poopstory.helpers.Box2DFactory;
import com.mobaxe.poopstory.managers.GameManager;

public class Pop implements Disposable {

	private Sprite sprite;
	public Body body;
	private World world;

	public Pop(World world, Sprite sprite, BodyType bodyType, Vector2 pos, Vector2 size, short filter) {

		this.sprite = sprite;
		this.sprite.setSize(size.x, size.y);
		this.sprite.setOriginCenter();
		this.world = world;

		Shape shape = Box2DFactory.createCircleShape(.45f);
		FixtureDef fixtureDef = Box2DFactory.createFixture(shape, 0.1f, 0.1f, 0.1f, false);
		fixtureDef.filter.categoryBits = filter;
		Body body = Box2DFactory.createBody(world, bodyType, fixtureDef, pos, false);

		this.body = body;
	}

	public void draw(SpriteBatch batch) {
		if (!GameManager.isGameStarted) {
			sprite.setPosition(body.getPosition().x - 0.5f, body.getPosition().y - 0.5f);
			sprite.rotate(-1.5f);
			sprite.draw(batch);
		}
	}

	@Override
	public void dispose() {
		world.destroyBody(body);
	}

}
