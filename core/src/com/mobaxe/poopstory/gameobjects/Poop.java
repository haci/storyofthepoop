package com.mobaxe.poopstory.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.utils.Disposable;
import com.mobaxe.poopstory.helpers.AnimatedSprite;
import com.mobaxe.poopstory.helpers.Box2DFactory;
import com.mobaxe.poopstory.managers.GameManager;

public class Poop implements Disposable {

	public Body body;
	public Body head;
	public AnimatedSprite sprite;
	private World world;
	private Sprite stop;
	private int remainingJumpSteps;
	private boolean onGround;
	private boolean moving;
	private int direction;// -1 LEFT 1 RIGHT

	public Poop(World world, AnimatedSprite sprite, BodyType bodyType, Vector2 pos, Vector2 size, short filter) {

		this.sprite = sprite;
		this.sprite.setSize(size.x, size.y);
		this.sprite.setOriginCenter();
		this.world = world;
		TextureRegion[] regions = this.sprite.getAnimation().getKeyFrames();
		stop = new Sprite(regions[1]);
		stop.setSize(size.x, size.y);
		stop.setOriginCenter();

		Shape shape = Box2DFactory.createCircleShape(0.7f);
		FixtureDef fixtureDef = Box2DFactory.createFixture(shape, 1, 0.1f, 0.1f, false);
		fixtureDef.filter.categoryBits = filter;
		Body body = Box2DFactory.createBody(world, bodyType, fixtureDef, pos, false);
		this.body = body;

		Shape sh = Box2DFactory.createPolygonShape(0.9f, 0.5f);
		FixtureDef fd = Box2DFactory.createFixture(sh, 0.1f, 0, 0, false);
		fd.filter.categoryBits = filter;
		Body bdy = Box2DFactory.createBody(world, bodyType, fd, body.getPosition(), false);
		bdy.setFixedRotation(true);
		this.head = bdy;

		RevoluteJointDef rDef = new RevoluteJointDef();
		rDef.bodyA = this.head;
		rDef.bodyB = this.body;
		rDef.collideConnected = true;
		rDef.localAnchorA.y = -0.4f;
		world.createJoint(rDef);

	}

	public void draw(SpriteBatch batch) {
		handleInputs();
		handleSprites();
		jump(batch);
		move(batch);

	}

	private void move(SpriteBatch batch) {
		if (isMoving()) {
			setMoving(false);
			Vector2 velocity = body.getLinearVelocity();
			if (direction == -1) {
				velocity.x = -3.5f;
			} else {
				velocity.x = 3.5f;
			}
			body.setLinearVelocity(velocity.x, velocity.y);

			sprite.setPosition(body.getPosition().x - sprite.getWidth() / 2,
					body.getPosition().y - sprite.getHeight() / 3.8f);
			sprite.draw(batch);

		}
	}

	private void jump(SpriteBatch batch) {
		if (remainingJumpSteps > 0) {
			body.applyForce(new Vector2(0, 215), body.getWorldCenter(), true);
			remainingJumpSteps--;

			stop.setPosition(body.getPosition().x - stop.getWidth() / 2,
					body.getPosition().y - stop.getHeight() / 3.8f);
			stop.draw(batch);
			setOnGround(false);

		} else {
			if (isMoving() == false) {
				stop.setPosition(body.getPosition().x - stop.getWidth() / 2,
						body.getPosition().y - stop.getHeight() / 3.8f);
				stop.draw(batch);
				Vector2 velocity = body.getLinearVelocity();
				velocity.x = 0;
				body.setLinearVelocity(velocity.x, velocity.y);
			}
		}
	}

	private void handleSprites() {
		if (direction == -1) {
			sprite.setScale(-1, 1);
			stop.setScale(-1, 1);
		} else {
			sprite.setScale(1, 1);
			stop.setScale(1, 1);
		}
	}

	private void handleInputs() {
		if (isOnGround()) {
			if (Gdx.input.isKeyPressed(Keys.SPACE)) {
				remainingJumpSteps = 6;
			}
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			setMoving(true);
			direction = 1;
		}
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			setMoving(true);
			direction = -1;
		}
		if (isOnGround()) {
			if (GameManager.isJumpClicked) {
				remainingJumpSteps = 6;
			}
		}
		if (GameManager.isRightClicked) {
			setMoving(true);
			direction = 1;
		}
		if (GameManager.isLeftClicked) {
			setMoving(true);
			direction = -1;
		}

	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public boolean isMoving() {
		return moving;
	}

	public void setMoving(boolean moving) {
		this.moving = moving;
	}

	public boolean isOnGround() {
		return onGround;
	}

	public void setOnGround(boolean onGround) {
		this.onGround = onGround;
	}

	@Override
	public void dispose() {
		world.destroyBody(body);
	}

}
