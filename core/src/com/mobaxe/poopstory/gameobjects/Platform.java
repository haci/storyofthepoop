package com.mobaxe.poopstory.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.mobaxe.poopstory.managers.GameManager;

public class Platform implements Instance {

	private Sprite sprite;
	private Body instance;

	public Platform(Body body, Sprite sprite) {
		this.sprite = sprite;
		instance = body;
	}

	@Override
	public void draw(SpriteBatch batch) {
		if (instance != null) {
			sprite.draw(batch);
		}
	}

	@Override
	public void dispose(Body body) {
		if (body != null) {
			GameManager.world.destroyBody(body);
		}
	}

	public Body getInstance() {
		return instance;
	}
}
