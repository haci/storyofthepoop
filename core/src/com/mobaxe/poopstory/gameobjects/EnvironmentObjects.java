package com.mobaxe.poopstory.gameobjects;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import com.mobaxe.poopstory.helpers.BodyEditorLoader;
import com.mobaxe.poopstory.helpers.Box2DFactory;

public class EnvironmentObjects implements Disposable {

	public BodyEditorLoader bodyEditorLoader;
	public Body body;
	public Vector2 origin;
	public Sprite sprite;
	public Vector2 position;
	private FixtureDef fixtureDef;
	private BodyType bodyType;

	public EnvironmentObjects(FileHandle file, World world, Sprite sprite, BodyType bodyType, Vector2 pos,
			Vector2 spriteSize, float loaderScale, String name, short filter) {
		this.bodyType = bodyType;

		this.sprite = sprite == null ? null : sprite;

		bodyEditorLoader = new BodyEditorLoader(file);
		fixtureDef = Box2DFactory.createFixture(new PolygonShape(), 1, 0, 0, false);
		fixtureDef.filter.groupIndex = filter;
		body = Box2DFactory.createBody(world, this.bodyType, fixtureDef, pos, false);

		bodyEditorLoader.attachFixture(body, name, fixtureDef, loaderScale);
		origin = bodyEditorLoader.getOrigin(name, loaderScale);
		position = body.getPosition().sub(origin);

		if (this.sprite != null) {
			this.sprite.setSize(spriteSize.x, spriteSize.y);
			this.sprite.setOrigin(origin.x, origin.y);
			this.sprite.setPosition(position.x, position.y);
		}
	}

	public void draw(SpriteBatch batch) {
	}

	@Override
	public void dispose() {
	}
}
