package com.mobaxe.poopstory;

import com.badlogic.gdx.Game;
import com.mobaxe.poopstory.helpers.Assets;
import com.mobaxe.poopstory.managers.ScreenManager;
import com.mobaxe.poopstory.screens.MyScreens;

public class PoopStory extends Game {

	private static PoopPrefs prefs;

	public static ActionResolver actionResolver;

	public PoopStory(ActionResolver actionResolver) {
		PoopStory.actionResolver = actionResolver;
	}

	@Override
	public void create() {
		prefs = new PoopPrefs();

		Assets.loadTexturesOnCreate();
		ScreenManager.getInstance().initialize(this);
		ScreenManager.getInstance().show(MyScreens.SPLASH_SCREEN);
	}

	public static PoopPrefs getPrefs() {
		return prefs;
	}

	@Override
	public void dispose() {
		ScreenManager.getInstance().dispose();
		Assets.dispose();
	}
}
