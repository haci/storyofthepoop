package com.mobaxe.poopstory.ui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.poopstory.helpers.Assets;
import com.mobaxe.poopstory.managers.GameManager;
import com.mobaxe.poopstory.managers.ScreenManager;
import com.mobaxe.poopstory.screens.MyScreens;

public class LevelButtons extends Button {

	private String buttonUp;
	private Skin skin;
	private ButtonStyle style;
	private int level;
	
	public LevelButtons(int level) {
		this.level = level;
		buttonUp = "ButtonUp";
		initSkins();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();

		skin.add(buttonUp, Assets.levels.get(level - 1));

		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				setLevel();
				refreshScreen();
			}

		});
	}

	public void setLevel() {
		switch (level) {
		case 1:
			GameManager.setCurrentLevel(level);
			break;
		case 2:
			GameManager.setCurrentLevel(level);
			break;
		case 3:
			GameManager.setCurrentLevel(level);
			break;
		case 4:
			GameManager.setCurrentLevel(level);
			break;
		case 5:
			GameManager.setCurrentLevel(level);
			break;
		case 6:
			GameManager.setCurrentLevel(level);
			break;
		case 7:
			GameManager.setCurrentLevel(level);
			break;
		case 8:
			GameManager.setCurrentLevel(level);
			break;
		case 9:
			GameManager.setCurrentLevel(level);
			break;
		case 10:
			GameManager.setCurrentLevel(level);
			break;
		case 11:
			GameManager.setCurrentLevel(level);
			break;
		case 12:
			GameManager.setCurrentLevel(level);
			break;
		default:
			break;
		}
	}

	public void refreshScreen() {
		ScreenManager.getInstance().dispose(MyScreens.LEVEL_SCREEN);
		ScreenManager.getInstance().show(MyScreens.GAME_SCREEN);
	}

	public String getButton() {
		return level + "";
	}
}