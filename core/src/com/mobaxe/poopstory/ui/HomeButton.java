package com.mobaxe.poopstory.ui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.poopstory.helpers.Assets;
import com.mobaxe.poopstory.managers.ScreenManager;
import com.mobaxe.poopstory.screens.MyScreens;

public class HomeButton extends Button {

	private String buttonUp;
	private String buttonOver;
	private String buttonName = "Home";
	private Skin skin;
	private ButtonStyle style;

	public HomeButton() {
		buttonUp = buttonName + "ButtonUp";
		buttonOver = buttonName + "ButtonOver";
		initSkins();
		setButtonStyle();
		clickListener(buttonName);
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, Assets.homeBtn);
		skin.add(buttonOver, Assets.homeBtn);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.over = skin.getDrawable(buttonOver);
		style.down = skin.getDrawable(buttonOver);
		setStyle(style);
	}

	private void clickListener(final String buttonName) {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				ScreenManager.getInstance().dispose(MyScreens.GAME_OVER_SCREEN);
				ScreenManager.getInstance().show(MyScreens.LEVEL_SCREEN);
			}
		});
	}

}
