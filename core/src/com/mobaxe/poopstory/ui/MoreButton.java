package com.mobaxe.poopstory.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.poopstory.helpers.Assets;

public class MoreButton extends Button {

	private String buttonUp;
	private String buttonDown;
	private Skin skin;
	private ButtonStyle style;
	private String url = "market://search?q=<mobaxe>&c=apps";

	public MoreButton() {
		buttonUp = "ButtonUp";
		buttonDown = "ButtonDown";
		initSkins();
		setButtonStyle();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, Assets.dotsBtn);
		skin.add(buttonDown, Assets.dotsBtn);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.down = skin.getDrawable(buttonDown);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				buttonFunction(event);
			}

			private void buttonFunction(InputEvent event) {
				Gdx.net.openURI(url);
			}

		});
	}
}